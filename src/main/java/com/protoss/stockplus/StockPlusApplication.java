package com.protoss.stockplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockPlusApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockPlusApplication.class, args);
	}

}
