package com.protoss.stockplus.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.stockplus.entity.Customer;
import com.protoss.stockplus.service.CustomerService;



@RestController
public class CustomerController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;
	
	@GetMapping("/customer/id/{id}")
	public Customer getCustomer(@PathVariable Long id) {
		return customerService.getCustomerById(id);
	}
	
	@GetMapping("/customer/code/{code}")
	public Customer getCustomerByCode(@PathVariable String code) {
		return customerService.getCustomerByCode(code);
	}
	
	@PostMapping(value = "/customer/save")
	public ResponseEntity createCustomer(@ModelAttribute Customer customer) {
		customerService.save(customer);
		return new ResponseEntity(customer, HttpStatus.OK);
	}

}
