package com.protoss.stockplus.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.stockplus.entity.Item;
import com.protoss.stockplus.service.ItemService;



@RestController
public class ItemController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	ItemService itemService;
	
	@GetMapping("/items/{id}")
	public Item getItem(@PathVariable Long id) {
		return itemService.getItemById(id);
	}

	@GetMapping("/items/findByCriteria")
	public List findByCriteria(@RequestParam(name="name",required=false) String name,
			@RequestParam(name="itemCode",required=false) String itemCode,
			@RequestParam(name="barcode",required=false) String barcode,
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResults",required=false) String maxResults) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("name", name);
		criteriaMap.put("itemCode", itemCode);
		criteriaMap.put("barcode", barcode);
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResults", maxResults);
		
		return itemService.findByCriteria(criteriaMap);
	}
	
	@GetMapping("/items/findItemSize")
	public Map<String,Integer> findItemSize(@RequestParam(name="name",required=false) String name,
			@RequestParam(name="itemCode",required=false) String itemCode,
			@RequestParam(name="barcode",required=false) String barcode ) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("name", name);
		criteriaMap.put("itemCode", itemCode);
		criteriaMap.put("barcode", barcode);
		
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", itemService.findItemSize(criteriaMap));
		return mapSize;
	}
	
	@PostMapping(value = "/items/save")
	public ResponseEntity createItem(@ModelAttribute Item item) {
		itemService.save(item);
		return new ResponseEntity(item, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/items/{id}")
	public ResponseEntity deleteItem(@PathVariable Long id) {
		itemService.deleteItemById(id);
		return new ResponseEntity("", HttpStatus.OK);
	}
}
