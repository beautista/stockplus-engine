package com.protoss.stockplus.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParameterController {

	@GetMapping("/parameters")
	public List getParameter() {
		List<Map<String,Object>> resultList = new ArrayList<Map<String,Object>>();
		
		for(int i=0;i<100000;i++){
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			
			Map<String,Object> tcsp = new HashMap<String,Object>();
			tcsp.put("entry", "TCSP");
			tcsp.put("description", "SP Plant");
			list.add(tcsp);
			
			Map<String,Object> tcnk = new HashMap<String,Object>();
			tcnk.put("entry", "TCNK");
			tcnk.put("description", "NK Plant");
			list.add(tcnk);
			
			map.put("code", "P"+i);
			map.put("description", "Plant");
			map.put("parameterDetails", list);
			resultList.add(map);
		}
		
		
		
		
		return resultList;
	}
	
//	@GetMapping("/barcode")
//	public Map getBarcode(@RequestParam(name="barcode") String barcode,@RequestParam(name="sender") String sender) {
//		Map<String,Object> object = new HashMap<String,Object>();
//		
//		if(AppConstant.MAP_ITEM.get(barcode) != null){
//			object.put("Product", AppConstant.MAP_ITEM.get(barcode).getName());
//			object.put("Barcode", AppConstant.MAP_ITEM.get(barcode).getBarcode());
//			object.put("Quantity", AppConstant.MAP_ITEM.get(barcode).getQuantity());
//		}else{
//			object.put("Product", "No data");
//		}
//		
//		return object;
//	}
//	
//	@PostMapping("/barcode")
//	public void pushBarcode(@RequestParam(name="barcode") String barcode,
//			               @RequestParam(name="name") String name,
//			               @RequestParam(name="quantity") String quantity,
//			               HttpServletResponse response) {
//		
//		if(AppConstant.MAP_ITEM.get(barcode) == null){
//			Item item = new Item();
//			item.setBarcode(barcode);
//			item.setName(name);
//			item.setQuantity(Integer.parseInt(quantity));
//			AppConstant.MAP_ITEM.put(barcode, item);
//		}
//		
//	}
}
