package com.protoss.stockplus.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.protoss.stockplus.entity.Item;
import com.protoss.stockplus.entity.SalesTransaction;
import com.protoss.stockplus.service.ItemService;
import com.protoss.stockplus.service.SalesTransactionService;

@Controller
public class PosController {
	static final Logger LOGGER = LoggerFactory.getLogger(PosController.class);
	
	@Autowired
	ItemService itemService;
	
	@Autowired
	SalesTransactionService salesTransactionService;

	@GetMapping("/pos/mobileView")
	public String mobileview(@RequestParam(name="barcode",required=false) String barcode,
			                 @RequestParam(name="saler",required=false) String saler,
			                 HttpServletRequest request,
			                 HttpServletResponse response,
			                 Model uiModel) {
		
		Item item = itemService.getItemByBarcode(barcode);
		if(item!=null){
			uiModel.addAttribute("item", item);
			uiModel.addAttribute("saler", saler);
			return "posmobileview";
		}else{
			return "noitemfound";
		}
		
		
	}
	
	@PostMapping(value = "/pos/additem")
	public ResponseEntity addItem(@RequestParam(name="itemId",required=false) Long itemId,
                                  @RequestParam(name="saler",required=false) String saler,
                                  @RequestParam(name="quantity",required=false,defaultValue="1") Integer quantity,
                                  @RequestParam(name="flagQuantity",required=false,defaultValue="S") String flagQuantity) {
		
		Item item = itemService.getItemById(itemId);
		
		
		
		
		
		if(item!=null){
			if(quantity > item.getStockOnHand()){
				quantity = item.getStockOnHand();
			}
			
			//Set Selling Price
			Float sellingPriceAmount = item.getSellingPriceAmount();
			//Set Purchase Price
			Float purchasePriceAmount = item.getPurchasePriceAmount();
			//Set Price Type
			Float promotionSellingPriceAmount = item.getPromotionSellingPriceAmount();
			String priceType = "N"; //is normal
			if(sellingPriceAmount != null && promotionSellingPriceAmount!=null && sellingPriceAmount.equals(promotionSellingPriceAmount)){
				priceType = "P";//Promotion
			}
			
			//Set Total Amount
			Float totalAmount = quantity * sellingPriceAmount;
			
			//Set Total Amount
			Float totalCostAmount = quantity * purchasePriceAmount;
			
			SalesTransaction salesTransaction = null;
			
			Map<String,Object> criteriaMap = new HashMap<String,Object>();
			criteriaMap.put("itemCode", item.getItemCode());
			criteriaMap.put("saler", saler);
			
			List<SalesTransaction> transactionList = salesTransactionService.findByCriteria(criteriaMap);
			if(transactionList!=null && transactionList.size() >= 1){
				salesTransaction = transactionList.get(0);
				
				
				salesTransaction.setSellingPriceAmount(sellingPriceAmount);
				salesTransaction.setPriceType(priceType);
				
				//Set Quantity
				if("P".endsWith(flagQuantity)){
					quantity = salesTransaction.getQuantity()+quantity;
				}else if("N".endsWith(flagQuantity)){
					quantity = salesTransaction.getQuantity()-quantity;
				}
				if(quantity > item.getStockOnHand()){
					quantity = item.getStockOnHand();
				}
				
				totalAmount = quantity * sellingPriceAmount;
                totalCostAmount = quantity * purchasePriceAmount;
				
				salesTransaction.setTotalSellingPriceAmount(totalAmount);
				salesTransaction.setTotalPuechasePriceAmount(totalCostAmount);
				salesTransaction.setItemId(item.getId());
				salesTransaction.setItemCode(item.getItemCode());
				salesTransaction.setItemBarcode(item.getItemBarcode());
				salesTransaction.setItemName(item.getName());
				salesTransaction.setQuantity(quantity);
				salesTransaction.setSaler(saler);
			}else{
				salesTransaction = new SalesTransaction();
				salesTransaction.setSellingPriceAmount(sellingPriceAmount);
				salesTransaction.setPriceType(priceType);
				salesTransaction.setTotalSellingPriceAmount(totalAmount);
				salesTransaction.setTotalPuechasePriceAmount(totalCostAmount);
				salesTransaction.setItemId(item.getId());
				salesTransaction.setItemCode(item.getItemCode());
				salesTransaction.setItemBarcode(item.getItemBarcode());
				salesTransaction.setItemName(item.getName());
				salesTransaction.setQuantity(quantity);
				salesTransaction.setSaler(saler);
			}
			
			if(quantity > 0){
				salesTransactionService.save(salesTransaction);
			}
			
		}
		
		return new ResponseEntity("", HttpStatus.OK);
	}
	
	@PostMapping(value = "/pos/additembyapp")
	public ResponseEntity additembyapp(@RequestParam(name="itemCode",required=false,defaultValue="null") String itemCode,
								 @RequestParam(name="itemBarcode",required=false,defaultValue="null") String itemBarcode,
                                  @RequestParam(name="saler",required=false) String saler,
                                  @RequestParam(name="quantity",required=false,defaultValue="1") Integer quantity,
                                  @RequestParam(name="flagQuantity",required=false,defaultValue="S") String flagQuantity) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("itemCode", itemCode);
		criteriaMap.put("barcode", itemBarcode);
		List<Item> itemLs = null;
		if(!"null".equals(String.valueOf(itemCode)) || !"null".equals(String.valueOf(itemBarcode))){
			itemLs = itemService.findByCriteria(criteriaMap);
		}
		
		if(itemLs!=null && itemLs.size() >= 1){
			Item item = itemLs.get(0);
			if(quantity > item.getStockOnHand()){
				quantity = item.getStockOnHand();
			}
			
			//Set Selling Price
			Float sellingPriceAmount = item.getSellingPriceAmount();
			//Set Purchase Price
			Float purchasePriceAmount = item.getPurchasePriceAmount();
			//Set Price Type
			Float promotionSellingPriceAmount = item.getPromotionSellingPriceAmount();
			String priceType = "N"; //is normal
			if(sellingPriceAmount != null && promotionSellingPriceAmount!=null && sellingPriceAmount.equals(promotionSellingPriceAmount)){
				priceType = "P";//Promotion
			}
			//Set Total Amount
			Float totalAmount = quantity * sellingPriceAmount;
			//Set Total Amount
			Float totalCostAmount = quantity * purchasePriceAmount;
			
			
			SalesTransaction salesTransaction = null;
			
			Map<String,Object> criteria = new HashMap<String,Object>();
			criteria.put("itemCode", item.getItemCode());
			criteria.put("saler", saler);
			
			List<SalesTransaction> transactionList = salesTransactionService.findByCriteria(criteria);
			if(transactionList!=null && transactionList.size() >= 1){
				salesTransaction = transactionList.get(0);
				salesTransaction.setSellingPriceAmount(sellingPriceAmount);
				salesTransaction.setPriceType(priceType);
				
				//Set Quantity
				if("P".endsWith(flagQuantity)){
					quantity = salesTransaction.getQuantity()+quantity;
				}else if("N".endsWith(flagQuantity)){
					quantity = salesTransaction.getQuantity()-quantity;
				}
				if(quantity > item.getStockOnHand()){
					quantity = item.getStockOnHand();
				}
				
				totalAmount = quantity * sellingPriceAmount;
				totalCostAmount = quantity * purchasePriceAmount;
				
				salesTransaction.setTotalSellingPriceAmount(totalAmount);
				salesTransaction.setTotalPuechasePriceAmount(totalCostAmount);
				salesTransaction.setItemId(item.getId());
				salesTransaction.setItemCode(item.getItemCode());
				salesTransaction.setItemBarcode(item.getItemBarcode());
				salesTransaction.setItemName(item.getName());
				salesTransaction.setQuantity(quantity);
				salesTransaction.setSaler(saler);
			}else{
				salesTransaction = new SalesTransaction();
				salesTransaction.setSellingPriceAmount(sellingPriceAmount);
				salesTransaction.setPriceType(priceType);
				salesTransaction.setTotalSellingPriceAmount(totalAmount);
				salesTransaction.setTotalPuechasePriceAmount(totalCostAmount);
				salesTransaction.setItemId(item.getId());
				salesTransaction.setItemCode(item.getItemCode());
				salesTransaction.setItemBarcode(item.getItemBarcode());
				salesTransaction.setItemName(item.getName());
				salesTransaction.setQuantity(quantity);
				salesTransaction.setSaler(saler);
			}
			
			if(quantity > 0){
				salesTransactionService.save(salesTransaction);
			}
			
		}
		return new ResponseEntity("", HttpStatus.OK);
		
	}
	
}
