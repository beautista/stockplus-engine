package com.protoss.stockplus.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.stockplus.entity.Item;
import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.entity.SaleOrderItems;
import com.protoss.stockplus.entity.SalesTransaction;
import com.protoss.stockplus.entity.StockHistory;
import com.protoss.stockplus.service.ItemService;
import com.protoss.stockplus.service.SalesOrderService;
import com.protoss.stockplus.service.SalesTransactionService;
import com.protoss.stockplus.service.StockHistoryService;

@RestController
public class PosRestController {

	static final Logger LOGGER = LoggerFactory.getLogger(PosRestController.class);
		
	@Autowired
	SalesOrderService salesOrderService;
	

	@Autowired
	SalesTransactionService salesTransactionService;
	

	@Autowired
	ItemService itemService;
	
	@Autowired
	StockHistoryService stockHistoryService;
	
	@PostMapping(value = "/pos/addSaleOrder")
	public ResponseEntity addSaleOrder(@ModelAttribute SaleOrder saleOrder) {
		SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdp = new SimpleDateFormat("yyyyMMdd");
		LOGGER.info("saleOrder={}",saleOrder);
		//Save SO
		saleOrder.setSaleDate(new Timestamp(System.currentTimeMillis()));
		saleOrder.setSaleDateString(sd.format(new Date()));
		saleOrder.setSalePeriod(sdp.format(new Date()));
		salesOrderService.save(saleOrder);
		LOGGER.info("saleOrder={}",saleOrder.getSoNumber());
		
		//Persist to detail
		List<SalesTransaction> salesTransactionLs = salesTransactionService.findBySaler(saleOrder.getCreatedBy());
		if(salesTransactionLs!=null) for(SalesTransaction salesTransaction:salesTransactionLs){
			SaleOrderItems model = new SaleOrderItems();
			model.setItemBarode(salesTransaction.getItemBarcode());
			model.setItemCode(salesTransaction.getItemCode());
			model.setItemName(salesTransaction.getItemName());
			model.setPriceType(salesTransaction.getPriceType());
			model.setSellingPriceAmount(salesTransaction.getSellingPriceAmount());
			model.setQuantity(salesTransaction.getQuantity());
			model.setTotalAmount(salesTransaction.getTotalSellingPriceAmount());
			model.setSoId(saleOrder.getId());
			model.setSoNumber(saleOrder.getSoNumber());
			model.setCreatedBy(saleOrder.getCreatedBy());
			model.setCreatedDate(saleOrder.getCreatedDate());
			model.setCustomerCode(saleOrder.getCustomerCode());
			salesOrderService.save(model);
			
			//Cut Stock
			Item item = itemService.getItemById(salesTransaction.getItemId());
			item.setStockOnHand(item.getStockOnHand() - salesTransaction.getQuantity());
			itemService.save(item);
			
			//Persist Stock History
			StockHistory stockHistory = new StockHistory();
			stockHistory.setTransactionType(StockHistory.TRANSACTION_TYPE_OUT);
			stockHistory.setActivityType(StockHistory.ACTIVITY_TYPE_SALE_ORDER);
			stockHistory.setActivityNumber(saleOrder.getSoNumber());
			stockHistory.setQuantity(salesTransaction.getQuantity());
			stockHistory.setCreatedBy(saleOrder.getCreatedBy());
			stockHistory.setCreatedDate(saleOrder.getCreatedDate());
			stockHistoryService.save(stockHistory);
			
		}
		
		//Clear Transaction
		salesTransactionService.deleteSalesTransactionByUser(saleOrder.getCreatedBy());
		
		
		
		return new ResponseEntity(saleOrder, HttpStatus.OK);
	}
	
}
