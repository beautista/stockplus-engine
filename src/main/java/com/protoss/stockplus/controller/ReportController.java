package com.protoss.stockplus.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.stockplus.service.ReportService;
import com.protoss.stockplus.util.URLDecode;


@RestController
public class ReportController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	ReportService reportService;
	

	@GetMapping("/report/item/findByCriteria")
	public List findReportItemByCriteria(
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResults",required=false) String maxResults,
			@RequestParam(name="startDate",required=false) String startDate,
			@RequestParam(name="endDate",required=false) String endDate,
			@RequestParam(name="itemCode",required=false) String itemCode,
			@RequestParam(name="itemBarcode",required=false) String itemBarcode,
			@RequestParam(name="itemName",required=false) String itemName) throws UnsupportedEncodingException {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResults", maxResults);
		criteriaMap.put("startDate", startDate);
		criteriaMap.put("endDate", endDate);
		criteriaMap.put("itemCode", itemCode);
		criteriaMap.put("itemBarcode", itemBarcode);
		criteriaMap.put("itemName", URLDecode.decode(itemName));
		
		return reportService.findByCriteriaReportItem(criteriaMap);
	}
	

	
	@GetMapping("/report/item/findSize")
	public Map<String,Integer> findReportItemItemSize(
			@RequestParam(name="startDate",required=false) String startDate,
			@RequestParam(name="endDate",required=false) String endDate,
			@RequestParam(name="itemCode",required=false) String itemCode,
			@RequestParam(name="itemBarcode",required=false) String itemBarcode,
			@RequestParam(name="itemName",required=false) String itemName ) throws UnsupportedEncodingException {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("startDate", startDate);
		criteriaMap.put("endDate", endDate);
		criteriaMap.put("itemCode", itemCode);
		criteriaMap.put("itemBarcode", itemBarcode);
		criteriaMap.put("itemName", URLDecode.decode(itemName));
		
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", reportService.findItemSizeReportItem(criteriaMap));
		return mapSize;
	}
	

	@GetMapping("/report/sale/findByCriteria")
	public List findReportSaleByCriteria(
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResults",required=false) String maxResults,
			@RequestParam(name="startDate",required=false) String startDate,
			@RequestParam(name="endDate",required=false) String endDate,
			@RequestParam(name="saler",required=false) String saler){
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResults", maxResults);
		criteriaMap.put("startDate", startDate);
		criteriaMap.put("endDate", endDate);
		criteriaMap.put("saler", saler);
		
		return reportService.findByCriteriaReportSale(criteriaMap);
	}
	

	
	@GetMapping("/report/sale/findSize")
	public Map<String,Integer> findReportSaleSize(
			@RequestParam(name="startDate",required=false) String startDate,
			@RequestParam(name="endDate",required=false) String endDate,
			@RequestParam(name="saler",required=false) String saler)  {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("startDate", startDate);
		criteriaMap.put("endDate", endDate);
		criteriaMap.put("saler", saler);
		
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", reportService.findItemSizeReportSale(criteriaMap));
		return mapSize;
	}
	
	@GetMapping("/report/summary/findByCriteria")
	public List findReportSummaryByCriteria(
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResults",required=false) String maxResults,
			@RequestParam(name="year",required=false) String year){
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResults", maxResults);
		criteriaMap.put("year", year);
		
		return reportService.findByCriteriaReportSummary(criteriaMap);
	}
	

	
	@GetMapping("/report/summary/findSize")
	public Map<String,Integer> findReportSummarySize(
			@RequestParam(name="year",required=false) String year)  {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("year", year);
		
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", reportService.findItemSizeReportSummary(criteriaMap));
		return mapSize;
	}
}
