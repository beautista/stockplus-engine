package com.protoss.stockplus.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.service.SalesOrderService;


@RestController
public class SaleOrderController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(SaleOrderController.class);
	
	
	@Autowired
	SalesOrderService salesOrderService;
	

	@GetMapping("/saleorder/findByCriteria")
	public List findByCriteria(@RequestParam(name="soNumber",required=false) String soNumber,
			@RequestParam(name="saleDateString",required=false) String saleDateString,
			@RequestParam(name="customerCode",required=false) String customerCode,
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResults",required=false) String maxResults) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("soNumber", soNumber);
		criteriaMap.put("saleDateString", saleDateString);
		criteriaMap.put("customerCode", customerCode);
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResults", maxResults);
		
		
		return salesOrderService.findByCriteria(criteriaMap);
	}
	
	@GetMapping("/saleorder/findSize")
	public Map<String,Integer> findItemSize(@RequestParam(name="soNumber",required=false) String soNumber,
			@RequestParam(name="saleDateString",required=false) String saleDateString,
			@RequestParam(name="customerCode",required=false) String customerCode) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("soNumber", soNumber);
		criteriaMap.put("saleDateString", saleDateString);
		criteriaMap.put("customerCode", customerCode);
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", salesOrderService.findSaleOrderSize(criteriaMap));
		return mapSize;
	}


	@GetMapping("/saleorderitem/findByCriteria")
	public List findSaleOrderItemByCriteria(@RequestParam(name="soNumber",required=false) String soNumber,
			@RequestParam(name="soId",required=false) Long soId,
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResults",required=false) String maxResults) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("soNumber", soNumber);
		criteriaMap.put("soId", soId);
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResults", maxResults);
		
		return salesOrderService.findSaleOrderItemByCriteria(criteriaMap);
	}
	
	@GetMapping("/saleorderitem/findSize")
	public Map<String,Integer> findSaleOrderItemSize(@RequestParam(name="soNumber",required=false) String soNumber,
			@RequestParam(name="soId",required=false) Long soId) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("soNumber", soNumber);
		criteriaMap.put("soId", soId);
		
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", salesOrderService.findSaleOrderItemSize(criteriaMap));
		return mapSize;
	}
	

	@GetMapping("/saleorderitemcustomer/findByCriteria")
	public List findSaleOrderItemCustomerByCriteria(@RequestParam(name="customerCode",required=false) String customerCode,
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResults",required=false) String maxResults) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("customerCode", customerCode);
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResults", maxResults);
		
		return salesOrderService.findSaleOrderItemCustomerByCriteria(criteriaMap);
	}
	
	@GetMapping("/saleorderitemcustomer/findSize")
	public Map<String,Integer> findSaleOrderItemCustomerSize(@RequestParam(name="customerCode",required=false) String customerCode
			) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("customerCode", customerCode);
		
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", salesOrderService.findSaleOrderItemCustomerSize(criteriaMap));
		return mapSize;
	}
	
	@GetMapping("/saleorder/{soNumber}")
	public SaleOrder getSaleOrder(@PathVariable String soNumber) {
		return salesOrderService.getSaleOrderByNumber(soNumber);
	}
	

	@GetMapping("/saleorderitem/{soNumber}")
	public List getSaleOrderItem(@PathVariable String soNumber) {
		return salesOrderService.getSaleOrderItemByNumber(soNumber);
	}
	
}
