package com.protoss.stockplus.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.stockplus.entity.SalesTransaction;
import com.protoss.stockplus.service.SalesTransactionService;


@RestController
public class SalesTranactionController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(SalesTranactionController.class);
	
	@Autowired
	SalesTransactionService salesTransactionService;
	
	@GetMapping("/salesTransactionService/{id}")
	public SalesTransaction getSalesTransaction(@PathVariable Long id) {
		return salesTransactionService.getById(id);
	}

	@GetMapping("/salesTransactionService/findByCriteria")
	public List findByCriteria(@RequestParam(name="saler",required=false) String saler,
			@RequestParam(name="firstResult",required=false) String firstResult,
			@RequestParam(name="maxResult",required=false) String maxResult) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();
		criteriaMap.put("saler", saler);
		criteriaMap.put("firstResult", firstResult);
		criteriaMap.put("maxResult", maxResult);
		
		return salesTransactionService.findByCriteria(criteriaMap);
	}
	
	@GetMapping("/salesTransactionService/findSize")
	public Map<String,Integer> findSize(@RequestParam(name="saler",required=false) String saler) {
		Map<String,Object> criteriaMap = new HashMap<String,Object>();

		criteriaMap.put("saler", saler);
		
		Map<String,Integer> mapSize = new HashMap<String,Integer>();
		mapSize.put("size", salesTransactionService.findSize(criteriaMap));
		return mapSize;
	}
	
	
	@PostMapping(value = "/salesTransactionService/save")
	public ResponseEntity createItem(@ModelAttribute SalesTransaction salesTransaction) {
		salesTransactionService.save(salesTransaction);
		return new ResponseEntity(salesTransaction, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/salesTransactionService/{id}")
	public ResponseEntity deleteItem(@PathVariable Long id) {
		salesTransactionService.deleteSalesTransactionById(id);
		return new ResponseEntity("", HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/salesTransactionService/saler/{saler}")
	public ResponseEntity clearBySaler(@PathVariable String saler) {
		salesTransactionService.deleteSalesTransactionByUser(saler);
		return new ResponseEntity("", HttpStatus.OK);
	}
}
