package com.protoss.stockplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Data
@Entity
@Table(name="mst_customer")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Customer {


	private @Id @Column(name="ctm_id")
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	@Column(name="ctm_code")
	private String customerCode;
	
	@Column(name="ctm_barcode")
	private String customerBarcode;
	
	@Column(name="ctm_firstname")
	private String firstname;
	
	@Column(name="ctm_lastname")
	private String lastname;
	
	@Column(name="ctm_point")
	@ColumnDefault("'0'")
	private Integer point;
	
	@Column(name="ctm_created_user")
	private String createdBy;
	
	@Column(name="ctm_created_time")
	private Timestamp createdDate;
	
	@Column(name="ctm_update_user")
	private String updateBy;
	
	@Column(name="ctm_update_time")
	private Timestamp updateDate;

}
