package com.protoss.stockplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Data
@Entity
@Table(name="tns_item")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Item {


	private @Id @Column(name="itm_id")
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	@Column(name="itm_code")
	private String itemCode;
	
	@Column(name="itm_barcode")
	private String itemBarcode;
	
	@Column(name="itm_name")
	private String name;
	
	@Column(name="itm_stock")
	@ColumnDefault("'0'")
	private Integer stockOnHand;
	
	@Column(name="itm_min_stock")
	@ColumnDefault("'0'")
	private Integer minimumStock;

	@Column(name="itm_sell_price")
	@ColumnDefault("'0.0'")
	private Float sellingPriceAmount;
	
	@Column(name="itm_nor_sell_price")
	@ColumnDefault("'0.0'")
	private Float normalSellingPriceAmount;
	
	@Column(name="itm_pro_sell_price")
	@ColumnDefault("'0.0'")
	private Float promotionSellingPriceAmount;
	
	@Column(name="itm_pro_sell_price_pct")
	@ColumnDefault("'0.0'")
	private Float promotionSellingPricePercentage;
	
	@Column(name="itm_pur_price")
	@ColumnDefault("'0.0'")
	private Float purchasePriceAmount;
	
	@Column(name="itm_nor_pur_price")
	@ColumnDefault("'0.0'")
	private Float normalPurchasePriceAmount;
	
	@Column(name="itm_pro_pur_price")
	@ColumnDefault("'0.0'")
	private Float promotionPurchasePriceAmount;
	
	@Column(name="itm_pro_pur_price_pt")
	@ColumnDefault("'0.0'")
	private Float promotionPurchasePricePercentage;
	
	@Column(name="itm_created_user")
	private String createdBy;
	
	@Column(name="itm_created_time")
	private Timestamp createdDate;
	
	@Column(name="itm_update_user")
	private String updateBy;
	
	@Column(name="itm_update_time")
	private Timestamp updateDate;

}
