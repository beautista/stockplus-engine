package com.protoss.stockplus.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Immutable
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"itemcode"})
public class ReportItem  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "itemcode", updatable = false, nullable = false)
	private String itemCode;
	
	@Column(name = "barcode", updatable = false)
	private String itemBarcode;
	
	@Column(name = "itemname", updatable = false)
	private String itemName;
	
	@Column(name = "quantity", updatable = false)
	private String quantity;
	
	@Column(name = "stock", updatable = false)
	private String stockOnhand;
	
	@Column(name = "minstock", updatable = false)
	private String minimumStock;
	
	@Column(name = "saledate", updatable = false)
	private String saleDate;
	
	

	
	
	
}
