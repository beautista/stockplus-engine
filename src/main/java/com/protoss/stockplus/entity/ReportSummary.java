package com.protoss.stockplus.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Immutable
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"salemonthnumdisp"})
public class ReportSummary {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "salemonthnumdisp", updatable = false, nullable = false)
	private String saleMonthNumberOfDisplay;
	
	@Column(name = "monthname", updatable = false)
	private String monthName;
	
	@Column(name = "datayear", updatable = false)
	private String dataYear;
	
	@Column(name = "subtotalamount", updatable = false)
	private String subTotalAmount;
	
	@Column(name = "rebateamount", updatable = false)
	private String rebateAmount;
	
	@Column(name = "vatamount", updatable = false)
	private String vatAmount;
	
	@Column(name = "totalamount", updatable = false)
	private String totalAmount;
	
	@Column(name = "subtotalafterrebate", updatable = false)
	private String subTotalAfterRebate;
	
	@Column(name = "itemcost", updatable = false)
	private String itemCostAmount;
	
	@Column(name = "contamount", updatable = false)
	private String contritutionAmount;
	
	@Column(name = "marginamount", updatable = false)
	private String marginAmount;

	
}
