package com.protoss.stockplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Data
@Entity
@Table(name="tns_running_number")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class RunningNumber {


	private @Id @Column(name="rnn_id")
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	@Column(name="rnn_code")
	private String code;
	
	@Column(name="rnn_running")
	private Integer running;
	
	@Column(name="rnn_created_user")
	private String createdBy;
	
	@Column(name="rnn_created_time")
	private Timestamp createdDate;
	
	@Column(name="rnn_update_user")
	private String updateBy;
	
	@Column(name="rnn_update_time")
	private Timestamp updateDate;

}
