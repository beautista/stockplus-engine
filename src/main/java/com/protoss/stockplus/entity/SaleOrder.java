package com.protoss.stockplus.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Data
@Entity
@Table(name="tns_sale_order")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class SaleOrder {


	private @Id @Column(name="so_id")
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	@Column(name="so_number")
	private String soNumber;
	
	@Column(name="so_customer_code")
	private String customerCode;
	
	@Column(name="so_saler")
	private String saler;
	
	@JsonFormat(pattern="dd-MM-yyyy hh:mm",timezone="GMT+7")
	@Column(name="so_sale_time")
	private Timestamp saleDate;
	
	@Column(name="so_sale_date")
	private String saleDateString;
	
	@Column(name="so_sale_period")
	private String salePeriod;
	
	@Column(name="so_item_count")
	@ColumnDefault("'0'")
	private Integer itemCount;
	
	@Column(name="so_subtotal_amt")
	@ColumnDefault("'0.0'")
	private Float subTotalAmount;
	
	@Column(name="so_rebate_amt")
	@ColumnDefault("'0.0'")
	private Float rebateAmount;
	
	@Column(name="so_subtotal_amt_after_rebate")
	@ColumnDefault("'0.0'")
	private Float subTotalAfterRebateAmount;
	
	@Column(name="so_vat_amt")
	@ColumnDefault("'0.0'")
	private Float vatAmount;
	
	@Column(name="so_subtotal_amt_after_vat")
	@ColumnDefault("'0.0'")
	private Float subTotalAfterVatAmount;
	
	@Column(name="so_total_amt")
	@ColumnDefault("'0.0'")
	private Float totalAmount;
	
	@Column(name="so_tended_amt")
	@ColumnDefault("'0.0'")
	private Float tendedAmount;
	
	@Column(name="so_change_amt")
	@ColumnDefault("'0.0'")
	private Float changeAmount;

	@Column(name="so_round_type")
	private String roundType;
	
	@Column(name="so_payment_type")
	private String paymentType;
	
	@Column(name="so_item_cost_amt")
	@ColumnDefault("'0.0'")
	private Float itemCostAmount;
	
	@Column(name="so_fix_cost_pct")
	@ColumnDefault("'0.0'")
	private Float fixCostPercentage;
	
	@Column(name="so_fix_cost_amt")
	@ColumnDefault("'0.0'")
	private Float fixCostAmount;
	
	@Column(name="so_cont_pct")
	@ColumnDefault("'0.0'")
	private Float contributionPercentage;
	
	@Column(name="so_cont_amt")
	@ColumnDefault("'0.0'")
	private Float contributionAmount;
	
	@Column(name="so_margin_pct")
	@ColumnDefault("'0.0'")
	private Float marginPercentage;
	
	@Column(name="so_margin_amt")
	@ColumnDefault("'0.0'")
	private Float marginAmount;
	
	@Column(name="so_created_user")
	private String createdBy;
	
	@Column(name="so_created_time")
	private Timestamp createdDate;
	
	@Column(name="so_update_user")
	private String updateBy;
	
	@Column(name="so_update_time")
	private Timestamp updateDate;

}
