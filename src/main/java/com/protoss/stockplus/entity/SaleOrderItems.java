package com.protoss.stockplus.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Data
@Entity
@Table(name="tns_sale_order_items")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class SaleOrderItems {


	private @Id @Column(name="sod_id")
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	@Column(name="sod_so_id")
	private Long soId;
	
	@Column(name="sod_so_number")
	private String soNumber;
	
	@Column(name="sod_item_code")
	private String itemCode;
	
	@Column(name="sod_item_barcode")
	private String itemBarode;

	@Column(name="sod_item_name")
	private String itemName;
	
	@Column(name="so_selling_price_amt")
	@ColumnDefault("'0.0'")
	private Float sellingPriceAmount;
	
	@Column(name="so_quantity")
	@ColumnDefault("'0'")
	private Integer quantity;
	
	@Column(name="so_total_amt")
	@ColumnDefault("'0.0'")
	private Float totalAmount;
	
	@Column(name="sod_price_type")
	private String priceType;
	
	@Column(name="sod_created_user")
	private String createdBy;
	
	@JsonFormat(pattern="dd-MM-yyyy hh:mm",timezone="GMT+7")
	@Column(name="sod_created_time")
	private Timestamp createdDate;
	
	@Column(name="sod_update_user")
	private String updateBy;
	
	@Column(name="sod_update_time")
	private Timestamp updateDate;
	

	@Column(name="sod_customer_code")
	private String customerCode;

}
