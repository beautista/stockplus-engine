package com.protoss.stockplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Data
@Entity
@Table(name="tns_sales_transaction")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class SalesTransaction {


	private @Id @Column(name="sts_id")
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	@Column(name="sts_saler")
	private String saler;
	
	@Column(name="sts_item_id")
	private Long itemId;
	

	@Column(name="sts_item_code")
	private String itemCode;

	@Column(name="sts_item_barcode")
	private String itemBarcode;

	@Column(name="sts_item_name")
	private String itemName;
	

	@Column(name="sts_quantity")
	@ColumnDefault("'0'")
	private Integer quantity;
	
	@Column(name="sts_sell_price")
	@ColumnDefault("'0.0'")
	private Float sellingPriceAmount;
	

	@Column(name="sts_price_type")//,columnDefinition="COMMENT 'N=Normal , P=Promotion' "
	private String priceType;
	

	@Column(name="sts_total_sell_price")
	@ColumnDefault("'0.0'")
	private Float totalSellingPriceAmount;
	
	@Column(name="sts_total_pur_price")
	@ColumnDefault("'0.0'")
	private Float totalPuechasePriceAmount;

}
