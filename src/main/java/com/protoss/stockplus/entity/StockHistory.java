package com.protoss.stockplus.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Data
@Entity
@Table(name="tns_history_stock")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class StockHistory {
	public static final String TRANSACTION_TYPE_IN= "I";
	public static final String TRANSACTION_TYPE_OUT= "O";
	

	public static final String ACTIVITY_TYPE_SALE_ORDER= "SO";


	private @Id @Column(name="hts_id")
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	@Column(name="hts_transaction_type")//,columnDefinition="COMMENT 'I=In , O=Out' "
	private String transactionType;
	
	@Column(name="hts_activity_type")
	private String activityType;
	
	@Column(name="hts_activity_number")
	private String activityNumber;

	@Column(name="hts_quantity")
	@ColumnDefault("'0'")
	private Integer quantity;
	
	@Column(name="hts_created_user")
	private String createdBy;
	
	@Column(name="hts_created_time")
	private Timestamp createdDate;
	
	@Column(name="hts_update_user")
	private String updateBy;
	
	@Column(name="hts_update_time")
	private Timestamp updateDate;

}
