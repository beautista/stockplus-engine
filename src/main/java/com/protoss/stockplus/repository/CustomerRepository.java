package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.stockplus.entity.Customer;

import java.util.List;
import java.util.Map;

public interface CustomerRepository extends JpaSpecificationExecutor<Customer>,
        JpaRepository<Customer, Long>,
        PagingAndSortingRepository<Customer, Long>,
        CustomerRepositoryCustom {


	Customer findById(@Param("id")String id);
	Customer findByCustomerCode(@Param("customerCode")String customerCode);
	

}
