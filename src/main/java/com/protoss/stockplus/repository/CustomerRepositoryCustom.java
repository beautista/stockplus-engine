package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import com.protoss.stockplus.entity.Customer;

public interface CustomerRepositoryCustom {

	List<Customer> findByCriteria(Map<String,Object> criteriaMap);
	Integer findCustomerSize(Map<String,Object> criteriaMap);
	void saveCustomer(Customer customer);
}
