package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.protoss.stockplus.entity.Customer;
import com.protoss.stockplus.entity.RunningNumber;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomerRepositoryImpl implements CustomerRepositoryCustom{

	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<Customer> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		
		
		
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		CriteriaBuilder          builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> criteria = builder.createQuery(Customer.class);
		Root<Customer>              root = criteria.from(Customer.class);
		EntityType<Customer>        type = entityManager.getMetamodel().entity(Customer.class);
		 
		
		String customerCode = "";
		String customerBarcode = "";
		String firstname = "";
		if(!String.valueOf(criteriaMap.get("firstname")).equals("null")){
			firstname = "%"+String.valueOf(criteriaMap.get("firstname"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			customerCode = "%"+String.valueOf(criteriaMap.get("customerCode"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("customerBarcode")).equals("null")){
			customerBarcode = "%"+String.valueOf(criteriaMap.get("customerBarcode"))+"%";
		}
		
		criteria.where(
				builder.and(
						builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("firstname", String.class)
				                )
				            ), "%" + firstname.toLowerCase() + "%"
				        ),
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("customerCode", String.class)
				                )
				            ), "%" + customerCode.toLowerCase() + "%"
				        ), 
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("customerBarcode", String.class)
				                )
				            ), "%" + customerBarcode.toLowerCase() + "%"
				        )
				    )
		);
				
		criteria.orderBy(builder.desc(root.get("customerCode")));
		
		List<Customer> customers = entityManager
				.createQuery(criteria).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();
		return customers;
	}

	@Override
	public Integer findCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		CriteriaBuilder          builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long>     criteria = builder.createQuery(Long.class);
		Root<Customer>              root = criteria.from(Customer.class);
		EntityType<Customer>        type = entityManager.getMetamodel().entity(Customer.class);
		 
		
		String customerCode = "";
		String customerBarcode = "";
		String firstname = "";
		if(!String.valueOf(criteriaMap.get("firstname")).equals("null")){
			firstname = "%"+String.valueOf(criteriaMap.get("firstname"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			customerCode = "%"+String.valueOf(criteriaMap.get("customerCode"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("customerBarcode")).equals("null")){
			customerBarcode = "%"+String.valueOf(criteriaMap.get("customerBarcode"))+"%";
		}
		

		criteria.select(builder.count(criteria.from(Customer.class)));
		criteria.where(
				builder.and(
						builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("firstname", String.class)
				                )
				            ), "%" + firstname.toLowerCase() + "%"
				        ),
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("customerCode", String.class)
				                )
				            ), "%" + customerCode.toLowerCase() + "%"
				        ), 
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("customerBarcode", String.class)
				                )
				            ), "%" + customerBarcode.toLowerCase() + "%"
				        )
				    )
		);
		
		
				
		Long count = entityManager.createQuery(criteria).getSingleResult();
		return count.intValue();
	}

	@Override
	public void saveCustomer(Customer customer) {
		// TODO Auto-generated method stub
		String customerCode = null;
		if("null".equals(String.valueOf(customer.getCustomerCode())) ||
				"".equals(String.valueOf(customer.getCustomerCode())) ||
				"AUTO".equals(customer.getCustomerCode())){
			customerCode = getMaxCustomerCode();
			customer.setCustomerCode(customerCode);
		}
		
		if("null".equals(String.valueOf(customer.getCustomerBarcode())) ||
				"".equals(String.valueOf(customer.getCustomerBarcode())) ||
				"AUTO".equals(customer.getCustomerBarcode())){
			customer.setCustomerBarcode(customerCode);
		}
		
		if("null".equals(String.valueOf(customer.getPoint())) ||
				"".equals(String.valueOf(customer.getPoint())) ){
			customer.setPoint(0);
		}
		
		entityManager.persist(customer);
	}
	
	private synchronized String getMaxCustomerCode() {
		// TODO Auto-generated method stub
		
		CriteriaBuilder               builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<RunningNumber> criteria = builder.createQuery(RunningNumber.class);
		Root<RunningNumber>              root = criteria.from(RunningNumber.class);
		EntityType<RunningNumber>        type = entityManager.getMetamodel().entity(RunningNumber.class);
		 
		criteria.where(
				builder.and(
						builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("code", String.class)
				                )
				            ), "CUSTOMER_CODE"
				        )
				    )
		);
			
		List<RunningNumber> resultLs = entityManager.createQuery(criteria).getResultList();
		RunningNumber runningNumber = null;
        if(resultLs!=null && resultLs.size()>0){
        	runningNumber = resultLs.get(0);
        }
        
        if(runningNumber!=null){
        	runningNumber.setRunning(runningNumber.getRunning()+1);
        }else{
        	runningNumber = new RunningNumber();
        	runningNumber.setCode("CUSTOMER_CODE");
        	runningNumber.setRunning(1);
        }
    	//getHibernateTemplate().saveOrUpdate(runningNumber);
        //entityManager.persist(runningNumber);
		return String.format("%010d", runningNumber.getRunning());
	}

}
