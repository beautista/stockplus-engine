package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import com.protoss.stockplus.entity.Item;;

public interface ItemRepository extends JpaSpecificationExecutor<Item>,
JpaRepository<Item, Long>,
PagingAndSortingRepository<Item, Long>,
ItemRepositoryCustom {

	Item findById(@Param("id")String id);
	Item findByItemBarcode(@Param("itemBarcode")String itemBarcode);
}
