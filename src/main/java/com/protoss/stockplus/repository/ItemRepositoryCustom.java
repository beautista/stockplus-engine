package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import com.protoss.stockplus.entity.Item;

public interface ItemRepositoryCustom {

	List<Item> findByCriteria(Map<String,Object> criteriaMap);
	Integer findItemSize(Map<String,Object> criteriaMap);
	void saveItem(Item item);
}
