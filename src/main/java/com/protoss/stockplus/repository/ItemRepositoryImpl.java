package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import com.protoss.stockplus.entity.Item;
import com.protoss.stockplus.entity.RunningNumber;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ItemRepositoryImpl implements ItemRepositoryCustom{
	

	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<Item> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		CriteriaBuilder          builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Item> criteria = builder.createQuery(Item.class);
		Root<Item>              root = criteria.from(Item.class);
		EntityType<Item>        type = entityManager.getMetamodel().entity(Item.class);
		 

		String name = "";
		String itemCode = "";
		String barcode = "";
		if(!String.valueOf(criteriaMap.get("name")).equals("null")){
			name = "%"+String.valueOf(criteriaMap.get("name"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			itemCode = "%"+String.valueOf(criteriaMap.get("itemCode"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("barcode")).equals("null")){
			barcode = "%"+String.valueOf(criteriaMap.get("barcode"))+"%";
		}
		
		criteria.where(
				builder.and(
						builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("name", String.class)
				                )
				            ), "%" + name.toLowerCase() + "%"
				        ),
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("itemCode", String.class)
				                )
				            ), "%" + itemCode.toLowerCase() + "%"
				        ), 
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("barcode", String.class)
				                )
				            ), "%" + barcode.toLowerCase() + "%"
				        )
				    )
		);
				
		criteria.orderBy(builder.desc(root.get("itemCode")));
		
		List<Item> items = entityManager
				.createQuery(criteria).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();
		return items;
	}

	@Override
	public Integer findItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		CriteriaBuilder          builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long>     criteria = builder.createQuery(Long.class);
		Root<Item>              root = criteria.from(Item.class);
		EntityType<Item>        type = entityManager.getMetamodel().entity(Item.class);
		 
		
		String name = "";
		String itemCode = "";
		String barcode = "";
		if(!String.valueOf(criteriaMap.get("name")).equals("null")){
			name = "%"+String.valueOf(criteriaMap.get("name"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			itemCode = "%"+String.valueOf(criteriaMap.get("itemCode"))+"%";
		}
		if(!String.valueOf(criteriaMap.get("barcode")).equals("null")){
			barcode = "%"+String.valueOf(criteriaMap.get("barcode"))+"%";
		}
		
		criteria.where(
				builder.and(
						builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("name", String.class)
				                )
				            ), "%" + name.toLowerCase() + "%"
				        ),
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("itemCode", String.class)
				                )
				            ), "%" + itemCode.toLowerCase() + "%"
				        ), 
				        builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("barcode", String.class)
				                )
				            ), "%" + barcode.toLowerCase() + "%"
				        )
				    )
		);
		
		
				
		Long count = entityManager.createQuery(criteria).getSingleResult();
		return count.intValue();
	}

	@Override
	public void saveItem(Item item) {
		// TODO Auto-generated method stub
		String itemCode = null;
		if("null".equals(String.valueOf(item.getItemCode())) ||
				"".equals(String.valueOf(item.getItemCode())) ||
				"AUTO".equals(item.getItemCode())){
			itemCode = getMaxItemCode();
			item.setItemCode(itemCode);
		}
		

		if("null".equals(String.valueOf(item.getItemBarcode())) ||
				"".equals(String.valueOf(item.getItemBarcode())) ||
				"AUTO".equals(item.getItemBarcode())){
			item.setItemBarcode(String.format("%013d", Integer.parseInt(itemCode)));
		}
		
		if("null".equals(String.valueOf(item.getStockOnHand())) ||
				"".equals(String.valueOf(item.getStockOnHand())) ){
			item.setStockOnHand(0);
		}
		
		if("null".equals(String.valueOf(item.getMinimumStock())) ||
				"".equals(String.valueOf(item.getMinimumStock())) ){
			item.setMinimumStock(0);
		}
		
		if("null".equals(String.valueOf(item.getSellingPriceAmount())) ||
				"".equals(String.valueOf(item.getSellingPriceAmount())) ){
			item.setSellingPriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPurchasePriceAmount())) ||
				"".equals(String.valueOf(item.getPurchasePriceAmount())) ){
			item.setPurchasePriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getNormalSellingPriceAmount())) ||
				"".equals(String.valueOf(item.getNormalSellingPriceAmount())) ){
			item.setNormalSellingPriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionSellingPriceAmount())) ||
				"".equals(String.valueOf(item.getPromotionSellingPriceAmount())) ){
			item.setPromotionSellingPriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionSellingPricePercentage())) ||
				"".equals(String.valueOf(item.getPromotionSellingPricePercentage())) ){
			item.setPromotionSellingPricePercentage(0.0f);
		}
		if("null".equals(String.valueOf(item.getNormalPurchasePriceAmount())) ||
				"".equals(String.valueOf(item.getNormalPurchasePriceAmount())) ){
			item.setNormalPurchasePriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionPurchasePriceAmount())) ||
				"".equals(String.valueOf(item.getPromotionPurchasePriceAmount())) ){
			item.setPromotionPurchasePriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionPurchasePricePercentage())) ||
				"".equals(String.valueOf(item.getPromotionPurchasePricePercentage())) ){
			item.setPromotionPurchasePricePercentage(0.0f);
		}
		
		entityManager.persist(item);
	}
	
	private synchronized String getMaxItemCode() {
		// TODO Auto-generated method stub
		
		CriteriaBuilder               builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<RunningNumber> criteria = builder.createQuery(RunningNumber.class);
		Root<RunningNumber>              root = criteria.from(RunningNumber.class);
		EntityType<RunningNumber>        type = entityManager.getMetamodel().entity(RunningNumber.class);
		 
		criteria.where(
				builder.and(
						builder.like(
				            builder.lower(
				                root.get(
				                    type.getDeclaredSingularAttribute("code", String.class)
				                )
				            ), "ITEM_CODE"
				        )
				    )
		);
		List<RunningNumber> resultLs = entityManager.createQuery(criteria).getResultList();
		
		RunningNumber runningNumber = null;
        if(resultLs!=null && resultLs.size()>0){
        	runningNumber = resultLs.get(0);
        }
        
        if(runningNumber!=null){
        	runningNumber.setRunning(runningNumber.getRunning()+1);
        }else{
        	runningNumber = new RunningNumber();
        	runningNumber.setCode("ITEM_CODE");
        	runningNumber.setRunning(1);
        }

        //getHibernateTemplate().saveOrUpdate(runningNumber);
        //entityManager.persist(runningNumber);
		
		return String.format("%08d", runningNumber.getRunning());
	}

}
