package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.protoss.stockplus.entity.Item;
import com.protoss.stockplus.entity.ReportItem;;

public interface ReportItemRepository extends JpaSpecificationExecutor<ReportItem>,
JpaRepository<ReportItem, Long>,
PagingAndSortingRepository<ReportItem, Long>,
ReportItemRepositoryCustom {

}
