package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import com.protoss.stockplus.entity.Item;

public interface ReportItemRepositoryCustom {

	List findByCriteria(Map<String,Object> criteriaMap);
	Integer findSize(Map<String,Object> criteriaMap);
}
