package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.protoss.stockplus.entity.Item;
import com.protoss.stockplus.entity.ReportItem;
import com.protoss.stockplus.entity.RunningNumber;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReportItemRepositoryImpl implements ReportItemRepositoryCustom{
	

	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		
		int firstResult = 0;
		int maxResults = 10;
		
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportItem.class, "report");
		
		if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
			criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
		}
		if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
			criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
		}
		
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemBarcode")).equals("null")){
			criteria.add(Restrictions.ilike("itemBarcode", "%"+String.valueOf(criteriaMap.get("itemBarcode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemName")).equals("null")){
			criteria.add(Restrictions.ilike("itemName", "%"+String.valueOf(criteriaMap.get("itemName")+"%")));
		}
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("itemCode"),"itemCode")
	            .add(Projections.groupProperty("itemBarcode"),"itemBarcode")
	            .add(Projections.groupProperty("itemName"),"itemName")
	            .add(Projections.sum("quantity"),"quantity")
	            .add(Projections.groupProperty("stockOnhand"),"stockOnhand")
	            .add(Projections.groupProperty("minimumStock"),"minimumStock")
		
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportItem.class));
		criteria.addOrder(Order.desc("quantity"));
		List<ReportItem> result = (List<ReportItem>) hibernateTemplate.findByCriteria(criteria,firstResult,maxResults);
		hibernateTemplate = null;
		return result;
	}

	@Override
	public Integer findSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub

		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportItem.class, "report");
		
		if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
			criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
		}
		if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
			criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
		}
		
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemBarcode")).equals("null")){
			criteria.add(Restrictions.ilike("itemBarcode", "%"+String.valueOf(criteriaMap.get("itemBarcode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemName")).equals("null")){
			criteria.add(Restrictions.ilike("itemName", "%"+String.valueOf(criteriaMap.get("itemName")+"%")));
		}
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("itemCode"),"itemCode")
	            .add(Projections.groupProperty("itemBarcode"),"itemBarcode")
	            .add(Projections.groupProperty("itemName"),"itemName")
	            .add(Projections.sum("quantity"),"quantity")
	            .add(Projections.groupProperty("stockOnhand"),"stockOnhand")
	            .add(Projections.groupProperty("minimumStock"),"minimumStock")
		
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportItem.class));
		List<ReportItem> result = (List<ReportItem>) hibernateTemplate.findByCriteria(criteria);
		hibernateTemplate = null;
		return result.size();
	}

	
}
