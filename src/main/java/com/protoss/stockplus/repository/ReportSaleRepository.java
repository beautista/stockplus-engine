package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.protoss.stockplus.entity.ReportSale;;

public interface ReportSaleRepository extends JpaSpecificationExecutor<ReportSale>,
JpaRepository<ReportSale, Long>,
PagingAndSortingRepository<ReportSale, Long>,
ReportSaleRepositoryCustom {

}
