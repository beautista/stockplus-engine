package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;
import com.protoss.stockplus.entity.ReportSale;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReportSalesRepositoryImpl implements ReportSaleRepositoryCustom{
	

	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		int firstResult = 0;
		int maxResults = 10;
		
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportSale.class, "report");
		
		if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
			criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
		}
		if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
			criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
		}
		
		if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
			criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
		}
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("saler"),"saler")
	            .add(Projections.sum("subTotalAmount"),"subTotalAmount")
	            .add(Projections.sum("rebateAmount"),"rebateAmount")
	            .add(Projections.sum("vatAmount"),"vatAmount")
	            .add(Projections.sum("totalAmount"),"totalAmount")
	            .add(Projections.sum("subTotalAfterRebate"),"subTotalAfterRebate")
	            .add(Projections.sum("itemCostAmount"),"itemCostAmount")
	            .add(Projections.sum("contritutionAmount"),"contritutionAmount")
	            .add(Projections.sum("marginAmount"),"marginAmount")
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportSale.class));
		criteria.addOrder(Order.desc("totalAmount"));
		List<ReportSale> result = (List<ReportSale>) hibernateTemplate.findByCriteria(criteria,firstResult,maxResults);
		
		
		hibernateTemplate = null;
		return result;
	}

	@Override
	public Integer findSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub

		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportSale.class, "report");
		
		if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
			criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
		}
		if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
			criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
		}
		
		if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
			criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
		}
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("saler"),"saler")
	            .add(Projections.sum("subTotalAmount"),"subTotalAmount")
	            .add(Projections.sum("rebateAmount"),"rebateAmount")
	            .add(Projections.sum("vatAmount"),"vatAmount")
	            .add(Projections.sum("totalAmount"),"totalAmount")
	            .add(Projections.sum("subTotalAfterRebate"),"subTotalAfterRebate")
	            .add(Projections.sum("itemCostAmount"),"itemCostAmount")
	            .add(Projections.sum("contritutionAmount"),"contritutionAmount")
	            .add(Projections.sum("marginAmount"),"marginAmount")
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportSale.class));
		List<ReportSale> result = (List<ReportSale>) hibernateTemplate.findByCriteria(criteria);
		
		hibernateTemplate = null;
		return result.size();
	}

	
}
