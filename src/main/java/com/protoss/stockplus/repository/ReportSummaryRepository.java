package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.protoss.stockplus.entity.ReportSale;
import com.protoss.stockplus.entity.ReportSummary;;

public interface ReportSummaryRepository extends JpaSpecificationExecutor<ReportSummary>,
JpaRepository<ReportSummary, Long>,
PagingAndSortingRepository<ReportSummary, Long>,
ReportSummaryRepositoryCustom {

}
