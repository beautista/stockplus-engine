package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

public interface ReportSummaryRepositoryCustom {

	List findByCriteria(Map<String,Object> criteriaMap);
	Integer findSize(Map<String,Object> criteriaMap);
}
