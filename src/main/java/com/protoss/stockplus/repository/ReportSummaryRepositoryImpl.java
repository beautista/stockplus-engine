package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;
import com.protoss.stockplus.entity.ReportSummary;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReportSummaryRepositoryImpl implements ReportSummaryRepositoryCustom{
	

	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportSummary.class, "report");
		
		
		if(!String.valueOf(criteriaMap.get("year")).equals("null")){
			criteria.add(Restrictions.ilike("dataYear", "%"+String.valueOf(criteriaMap.get("year")+"%")));
		}
		
		criteria.add(Restrictions.isNotNull("dataYear"));
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("monthName"),"monthName")
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("subTotalAfterRebate"),"subTotalAfterRebate")
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("contritutionAmount"),"contritutionAmount")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportSummary.class));
		criteria.addOrder(Order.desc("saleMonthNumberOfDisplay"));
		List<ReportSummary> result = (List<ReportSummary>) hibernateTemplate.findByCriteria(criteria);
		
		hibernateTemplate = null;
		return result;
	}

	@Override
	public Integer findSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub

		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportSummary.class, "report");
		
		
		if(!String.valueOf(criteriaMap.get("year")).equals("null")){
			criteria.add(Restrictions.ilike("dataYear", "%"+String.valueOf(criteriaMap.get("year")+"%")));
		}
		

		criteria.add(Restrictions.isNotNull("dataYear"));
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("monthName"),"monthName")
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("subTotalAfterRebate"),"subTotalAfterRebate")
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("contritutionAmount"),"contritutionAmount")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportSummary.class));
		criteria.addOrder(Order.desc("saleMonthNumberOfDisplay"));
		List<ReportSummary> result = (List<ReportSummary>) hibernateTemplate.findByCriteria(criteria);
		
		hibernateTemplate = null;
		return result.size();
	}

	
}
