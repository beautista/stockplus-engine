package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.stockplus.entity.ReportSale;
import com.protoss.stockplus.entity.ReportSummary;
import com.protoss.stockplus.entity.SaleOrderItems;

public interface SaleOrderItemsRepository extends JpaSpecificationExecutor<SaleOrderItems>,
JpaRepository<SaleOrderItems, Long>,
PagingAndSortingRepository<SaleOrderItems, Long>,
SaleOrderItemsRepositoryCustom {

	SaleOrderItems findById(@Param("id")String id);
}
