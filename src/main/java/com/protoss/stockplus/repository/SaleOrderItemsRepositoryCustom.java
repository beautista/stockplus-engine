package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.entity.SaleOrderItems;


public interface SaleOrderItemsRepositoryCustom {

	List<SaleOrderItems> findSaleOrderItemByCriteria(Map<String,Object> criteriaMap);
	Integer findSaleOrderItemSize(Map<String,Object> criteriaMap);
	List<SaleOrderItems> findSaleOrderItemCustomerByCriteria(Map<String,Object> criteriaMap);
	Integer findSaleOrderItemCustomerSize(Map<String,Object> criteriaMap);
	List<SaleOrderItems> getSaleOrderItemByNumber(String number);
	List<SaleOrderItems> findSaleOrderItemBySoNumber(String soNumber);
}
