package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.protoss.stockplus.entity.RunningNumber;
import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.entity.SaleOrderItems;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SaleOrderItemsRepositoryImpl implements SaleOrderItemsRepositoryCustom{
	

	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public List<SaleOrderItems> findSaleOrderItemByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(criteriaMap.get("soId")).equals("null")){
			criteria.add(Restrictions.eq("soId", criteriaMap.get("soId")));
		}
		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		
		List<SaleOrderItems> result = (List<SaleOrderItems>) hibernateTemplate.findByCriteria(criteria,firstResult,maxResults);
		hibernateTemplate = null;
		return null;
	}

	@Override
	public Integer findSaleOrderItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(criteriaMap.get("soId")).equals("null")){
			criteria.add(Restrictions.eq("soId", criteriaMap.get("soId")));
		}
		
		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		hibernateTemplate = null;
		return result;
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemCustomerByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		criteria.addOrder(Order.desc("createdDate"));
		criteria.addOrder(Order.desc("itemCode"));
		
		
		List<SaleOrderItems> result = (List<SaleOrderItems>) hibernateTemplate.findByCriteria(criteria,firstResult,maxResults);
		hibernateTemplate = null;
		return result;
	}

	@Override
	public Integer findSaleOrderItemCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		
		hibernateTemplate = null;
		return result;
	}

	@Override
	public List<SaleOrderItems> getSaleOrderItemByNumber(String number) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(number).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+number+"%"));
		}
		
		
		List<SaleOrderItems> result = (List<SaleOrderItems>) hibernateTemplate.findByCriteria(criteria);
		
		hibernateTemplate = null;
		return result;
	}


	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoNumber(String soNumber) {
		// TODO Auto-generated method stub
		//Session session = entityManager.unwrap(org.hibernate.SessionFactory.class);
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrder");
		
        criteria.add(Restrictions.ilike("soNumber", soNumber));
       
        List<SaleOrderItems> result = (List<SaleOrderItems>) hibernateTemplate.findByCriteria(criteria);
		
		hibernateTemplate = null;
		return result;
	}

	
	
	
	
}
