package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.stockplus.entity.ReportSale;
import com.protoss.stockplus.entity.ReportSummary;
import com.protoss.stockplus.entity.SaleOrder;;

public interface SaleOrderRepository extends JpaSpecificationExecutor<SaleOrder>,
JpaRepository<SaleOrder, Long>,
PagingAndSortingRepository<SaleOrder, Long>,
SaleOrderRepositoryCustom {

	SaleOrder findById(@Param("id")String id);
}
