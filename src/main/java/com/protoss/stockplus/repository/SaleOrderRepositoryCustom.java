package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.entity.SaleOrderItems;


public interface SaleOrderRepositoryCustom {

	SaleOrder getSaleOrderByNumber(String number) ;
	List findByCriteria(Map<String,Object> criteriaMap);
	Integer findSaleOrderSize(Map<String,Object> criteriaMap);
	void saveSaleOrder(SaleOrder saleOrder);
	void deleteSaleOrderById(Long id);
	
	List<SaleOrderItems> findSaleOrderItemBySoId(Long soId);
}
