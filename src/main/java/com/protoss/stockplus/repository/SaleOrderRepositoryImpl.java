package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.protoss.stockplus.entity.RunningNumber;
import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.entity.SaleOrderItems;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SaleOrderRepositoryImpl implements SaleOrderRepositoryCustom{
	

	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public List findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrder.class, "saleOrder");
		
		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		

		if(!String.valueOf(criteriaMap.get("saleDateString")).equals("null")){
			criteria.add(Restrictions.ilike("saleDateString", "%"+String.valueOf(criteriaMap.get("saleDateString")+"%")));
		}
		

		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		/*when set projection */
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("id"),"id")
	            .add(Projections.property("soNumber"),"soNumber")
	            .add(Projections.property("saler"),"saler")   
	            .add(Projections.property("saleDate"),"saleDate") 
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("itemCount"),"itemCount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            .add(Projections.property("customerCode"),"customerCode")
	            
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("subTotalAfterRebateAmount"),"subTotalAfterRebateAmount")
	            .add(Projections.property("subTotalAfterVatAmount"),"subTotalAfterVatAmount")
	            .add(Projections.property("tendedAmount"),"tendedAmount")
	            .add(Projections.property("changeAmount"),"changeAmount")
	            .add(Projections.property("roundType"),"roundType")
	            .add(Projections.property("paymentType"),"paymentType")
	            
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("fixCostPercentage"),"fixCostPercentage")
	            .add(Projections.property("fixCostAmount"),"fixCostAmount")
	            .add(Projections.property("contributionPercentage"),"contributionPercentage")
	            .add(Projections.property("contributionAmount"),"contributionAmount")
	            .add(Projections.property("marginPercentage"),"marginPercentage")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            
	            
	            );   
		criteria.setResultTransformer( Transformers.aliasToBean(SaleOrder.class));
		
		
		criteria.addOrder(Order.desc("soNumber"));
		List<SaleOrder> result = (List<SaleOrder>) hibernateTemplate.findByCriteria(criteria,firstResult,maxResults);
		
		hibernateTemplate = null;
		return result;
	}

	@Override
	public Integer findSaleOrderSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrder.class, "saleOrder");
		
		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("saleDateString")).equals("null")){
			criteria.add(Restrictions.ilike("saleDateString", "%"+String.valueOf(criteriaMap.get("saleDateString")+"%")));
		}


		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		hibernateTemplate = null;
		return result;

	}

	@Override
	public void saveSaleOrder(SaleOrder saleOrder) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		//Save Header
				String soNumber = "";
				if("null".equals(String.valueOf(saleOrder.getSoNumber())) ||
						"".equals(String.valueOf(saleOrder.getSoNumber())) ||
						"AUTO".equals(saleOrder.getSoNumber())){
					soNumber = getMaxSoNumber();
					saleOrder.setSoNumber(soNumber);
				}
				
		//Save Detail
		hibernateTemplate.save(saleOrder);
		hibernateTemplate = null;
	}
	
	private synchronized String getMaxSoNumber() {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(RunningNumber.class, "running");
		criteria.add(Restrictions.ilike("code", "SO_NUMBER"));
		List<RunningNumber> requltLs =  (List<RunningNumber>) hibernateTemplate.findByCriteria(criteria);
		RunningNumber runningNumber = null;
        if(requltLs!=null && requltLs.size()>0){
        	runningNumber = requltLs.get(0);
        }
        
        if(runningNumber!=null){
        	runningNumber.setRunning(runningNumber.getRunning()+1);
        }else{
        	runningNumber = new RunningNumber();
        	runningNumber.setCode("SO_NUMBER");
        	runningNumber.setRunning(1);
        }

        hibernateTemplate.saveOrUpdate(runningNumber);
        hibernateTemplate = null;
		
		return String.format("%06d", runningNumber.getRunning());
	}

	@Override
	public void deleteSaleOrderById(Long id) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		//Clear Header
		hibernateTemplate.deleteAll(findSaleOrderItemBySoId(id));

		//Clear Detail
		hibernateTemplate.delete(hibernateTemplate.get(SaleOrder.class, id));
		
		hibernateTemplate = null;
	}

	@Override
	public SaleOrder getSaleOrderByNumber(String number) {
		// TODO Auto-generated method stub

		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrder.class, "saleOrder");
		
        criteria.add(Restrictions.ilike("soNumber", number));
        
        /*when set projection */
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("id"),"id")
	            .add(Projections.property("soNumber"),"soNumber")
	            .add(Projections.property("saler"),"saler")   
	            .add(Projections.property("saleDate"),"saleDate") 
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("itemCount"),"itemCount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("subTotalAfterRebateAmount"),"subTotalAfterRebateAmount")
	            .add(Projections.property("subTotalAfterVatAmount"),"subTotalAfterVatAmount")
	            .add(Projections.property("tendedAmount"),"tendedAmount")
	            .add(Projections.property("changeAmount"),"changeAmount")
	            .add(Projections.property("roundType"),"roundType")
	            .add(Projections.property("paymentType"),"paymentType")
	            
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("fixCostPercentage"),"fixCostPercentage")
	            .add(Projections.property("fixCostAmount"),"fixCostAmount")
	            .add(Projections.property("contributionPercentage"),"contributionPercentage")
	            .add(Projections.property("contributionAmount"),"contributionAmount")
	            .add(Projections.property("marginPercentage"),"marginPercentage")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            
	            
	            );   
		criteria.setResultTransformer( Transformers.aliasToBean(SaleOrder.class));
        
        List<SaleOrder> requltLs =  (List<SaleOrder>) hibernateTemplate.findByCriteria(criteria);
        SaleOrder itemReturn = null;
        if(requltLs!=null && requltLs.size()>0){
        	itemReturn = requltLs.get(0);
        }
		
        hibernateTemplate = null;
		return itemReturn;
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoId(Long soId) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
        DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrder");
		
        criteria.add(Restrictions.ilike("soId", soId));
       
        List<SaleOrderItems>  result = (List<SaleOrderItems>) hibernateTemplate.findByCriteria(criteria);
        hibernateTemplate = null;
		return result;
	}
	
	
	
}
