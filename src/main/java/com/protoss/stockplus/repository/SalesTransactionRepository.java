package com.protoss.stockplus.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.stockplus.entity.Customer;
import com.protoss.stockplus.entity.SalesTransaction;;

public interface SalesTransactionRepository extends JpaSpecificationExecutor<SalesTransaction>,
JpaRepository<SalesTransaction, Long>,
PagingAndSortingRepository<SalesTransaction, Long>,
SalesTransactionRepositoryCustom {

	List<SalesTransaction>  findBySaler(@Param("saler")String saler);
}
