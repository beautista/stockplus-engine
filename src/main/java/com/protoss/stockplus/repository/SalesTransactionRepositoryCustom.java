package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;

import com.protoss.stockplus.entity.SalesTransaction;


public interface SalesTransactionRepositoryCustom {

	List findByCriteria(Map<String,Object> criteriaMap);
	Integer findSize(Map<String, Object> criteriaMap);
	void deleteSalesTransactionByUser(String saler);
}
