package com.protoss.stockplus.repository;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.protoss.stockplus.entity.RunningNumber;
import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.entity.SaleOrderItems;
import com.protoss.stockplus.entity.SalesTransaction;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SalesTransactionRepositoryImpl implements SalesTransactionRepositoryCustom{
	

	@PersistenceContext
    private EntityManager entityManager;


	@Override
	public List findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		int firstResult = 0;
		int maxResult = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResult")).equals("null")){
			maxResult = Integer.parseInt(String.valueOf(criteriaMap.get("maxResult")));
		}
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(SalesTransaction.class, "tx");
		
		if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
			criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
		}

		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		
		criteria.addOrder(Order.desc("id"));
		List<SalesTransaction> result = (List<SalesTransaction>) hibernateTemplate.findByCriteria(criteria,firstResult,maxResult);
		hibernateTemplate = null;
		
		return result;
	}

	@Override
	public Integer findSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SalesTransaction.class, "item");
		
		if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
			criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
		}
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		
		return result;
	}

	@Override
	public void deleteSalesTransactionByUser(String saler) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		
		Integer result = hibernateTemplate.execute(new HibernateCallback<Integer>() {

			@Override
			public Integer doInHibernate(Session session) throws HibernateException {
				// TODO Auto-generated method stub
				Query queryDelete = session.createQuery("DELETE SalesTransaction WHERE saler='"+saler+"'");
				return queryDelete.executeUpdate();
			}
			
		});
	}

	
	
	
}
