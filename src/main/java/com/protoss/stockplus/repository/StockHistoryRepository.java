package com.protoss.stockplus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.protoss.stockplus.entity.StockHistory;

public interface StockHistoryRepository extends JpaSpecificationExecutor<StockHistory>,
JpaRepository<StockHistory, Long>,
PagingAndSortingRepository<StockHistory, Long>{

	
}
