package com.protoss.stockplus.service;

import java.util.List;
import java.util.Map;

import com.protoss.stockplus.entity.Item;


public interface ItemService {

	public Item getItemById(Long id);
	public List<Item> findByCriteria(Map<String,Object> criteriaMap);
	public Integer findItemSize(Map<String,Object> criteriaMap);
	public Item getItemByBarcode(String barcode);
	public void save(Item item);
	public void deleteItemById(Long id);
}
