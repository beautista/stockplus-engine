package com.protoss.stockplus.service;

import java.util.List;
import java.util.Map;


public interface ReportService {

	public List findByCriteriaReportItem(Map<String,Object> criteriaMap);
	public Integer findItemSizeReportItem(Map<String,Object> criteriaMap);
	public List findByCriteriaReportSale(Map<String,Object> criteriaMap);
	public Integer findItemSizeReportSale(Map<String,Object> criteriaMap);
	public Integer findItemSizeReportSummary(Map<String,Object> criteriaMap);
	public List findByCriteriaReportSummary(Map<String,Object> criteriaMap);
	
}
