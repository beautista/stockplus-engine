package com.protoss.stockplus.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.stockplus.entity.Customer;
import com.protoss.stockplus.repository.CustomerRepository;
import com.protoss.stockplus.service.CustomerService;


@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	
	@Override
	public Customer getCustomerById(Long id) {
		// TODO Auto-generated method stub
		return customerRepository.getOne(id);
	}

	@Override
	public List<Customer> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return customerRepository.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return customerRepository.findCustomerSize(criteriaMap);
	}

	@Override
	public Customer getCustomerByCode(String code) {
		// TODO Auto-generated method stub
		return customerRepository.findByCustomerCode(code);
	}

	@Override
	public void save(Customer customer) {
		// TODO Auto-generated method stub
		customerRepository.saveCustomer(customer);
	}

	@Override
	public void deleteCustomerById(Long id) {
		// TODO Auto-generated method stub
		customerRepository.deleteById(id);
	}

	

}
