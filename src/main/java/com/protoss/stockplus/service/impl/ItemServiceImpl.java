package com.protoss.stockplus.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.stockplus.entity.Item;
import com.protoss.stockplus.repository.ItemRepository;
import com.protoss.stockplus.service.ItemService;


@Service
public class ItemServiceImpl implements ItemService{


	@Autowired
	ItemRepository itemRepository;
	
	@Override
	public Item getItemById(Long id) {
		// TODO Auto-generated method stub
		return itemRepository.getOne(id);
	}

	@Override
	public List<Item> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return itemRepository.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return itemRepository.findItemSize(criteriaMap);
	}

	@Override
	public Item getItemByBarcode(String barcode) {
		// TODO Auto-generated method stub
		return itemRepository.findByItemBarcode(barcode);
	}

	@Override
	public void save(Item item) {
		// TODO Auto-generated method stub
		itemRepository.saveItem(item);
	}

	@Override
	public void deleteItemById(Long id) {
		// TODO Auto-generated method stub
		itemRepository.deleteById(id);
	}

}
