package com.protoss.stockplus.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.stockplus.repository.ReportItemRepository;
import com.protoss.stockplus.repository.ReportSaleRepository;
import com.protoss.stockplus.repository.ReportSummaryRepository;
import com.protoss.stockplus.service.ReportService;


@Service
public class ReportServiceImpl implements ReportService {
	

	@Autowired
	ReportItemRepository reportItemRepository;
	
	@Autowired
	ReportSaleRepository reportSaleRepository;

	@Autowired
	ReportSummaryRepository reportSummaryRepository;

	@Override
	public List findByCriteriaReportItem(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportItemRepository.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findItemSizeReportItem(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportItemRepository.findSize(criteriaMap);
	}

	@Override
	public List findByCriteriaReportSale(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportSaleRepository.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findItemSizeReportSale(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportSaleRepository.findSize(criteriaMap);
	}

	@Override
	public List findByCriteriaReportSummary(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportSummaryRepository.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findItemSizeReportSummary(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportSummaryRepository.findSize(criteriaMap);
	}

}
