package com.protoss.stockplus.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.stockplus.entity.SaleOrder;
import com.protoss.stockplus.entity.SaleOrderItems;
import com.protoss.stockplus.repository.SaleOrderItemsRepository;
import com.protoss.stockplus.repository.SaleOrderRepository;
import com.protoss.stockplus.service.SalesOrderService;


@Service
public class SalesOrderServiceImpl implements SalesOrderService {

	@Autowired
	SaleOrderRepository saleOrderRepository;
	
	@Autowired
	SaleOrderItemsRepository saleOrderItemsRepository;
	
	@Override
	public SaleOrder getSaleOrderById(Long id) {
		// TODO Auto-generated method stub
		return saleOrderRepository.getOne(id);
	}

	@Override
	public List<SaleOrder> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return saleOrderRepository.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findSaleOrderSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return saleOrderRepository.findSaleOrderSize(criteriaMap);
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return saleOrderItemsRepository.findSaleOrderItemByCriteria(criteriaMap);
	}

	@Override
	public Integer findSaleOrderItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return saleOrderItemsRepository.findSaleOrderItemSize(criteriaMap);
	}
	

	@Override
	public List<SaleOrderItems> findSaleOrderItemCustomerByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return saleOrderItemsRepository.findSaleOrderItemCustomerByCriteria(criteriaMap);
	}

	@Override
	public Integer findSaleOrderItemCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return saleOrderItemsRepository.findSaleOrderItemCustomerSize(criteriaMap);
	}

	@Override
	public SaleOrder getSaleOrderByNumber(String number) {
		// TODO Auto-generated method stub
		return saleOrderRepository.getSaleOrderByNumber(number);
	}

	@Override
	public void save(SaleOrder saleOrder) {
		// TODO Auto-generated method stub

		saleOrderRepository.saveSaleOrder(saleOrder);
	}

	@Override
	public void save(SaleOrderItems saleOrderItems) {
		// TODO Auto-generated method stub

		saleOrderItemsRepository.save(saleOrderItems);
	}

	@Override
	public void deleteSaleOrderById(Long id) {
		// TODO Auto-generated method stub

		saleOrderRepository.deleteSaleOrderById(id);
	}

	@Override
	public void deleteSaleOrderItemsById(Long id) {
		// TODO Auto-generated method stub

		saleOrderItemsRepository.deleteById(id);
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoNumber(String soNumber) {
		// TODO Auto-generated method stub
		return saleOrderItemsRepository.findSaleOrderItemBySoNumber(soNumber);
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoId(Long soId) {
		// TODO Auto-generated method stub
		return saleOrderRepository.findSaleOrderItemBySoId(soId);
	}

	@Override
	public List<SaleOrderItems> getSaleOrderItemByNumber(String number) {
		// TODO Auto-generated method stub
		return saleOrderItemsRepository.getSaleOrderItemByNumber(number);
	}

}
