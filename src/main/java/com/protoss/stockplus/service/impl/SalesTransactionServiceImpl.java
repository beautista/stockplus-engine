package com.protoss.stockplus.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.stockplus.entity.SalesTransaction;
import com.protoss.stockplus.repository.SalesTransactionRepository;
import com.protoss.stockplus.service.SalesTransactionService;

@Service
public class SalesTransactionServiceImpl implements SalesTransactionService{


	@Autowired
	SalesTransactionRepository salesTransactionRepository;

	@Override
	public SalesTransaction getById(Long id) {
		// TODO Auto-generated method stub
		return salesTransactionRepository.getOne(id);
	}

	@Override
	public List<SalesTransaction> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesTransactionRepository.findByCriteria(criteriaMap);
	}

	@Override
	public void save(SalesTransaction salesTransaction) {
		// TODO Auto-generated method stub
		salesTransactionRepository.save(salesTransaction);
	}

	@Override
	public void deleteSalesTransactionById(Long id) {
		// TODO Auto-generated method stub
		salesTransactionRepository.deleteById(id);
	}

	@Override
	public Integer findSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesTransactionRepository.findSize(criteriaMap);
	}

	@Override
	public void deleteSalesTransactionByUser(String saler) {
		// TODO Auto-generated method stub
		salesTransactionRepository.deleteSalesTransactionByUser(saler);
	}

	@Override
	public List<SalesTransaction> findBySaler(String saler) {
		// TODO Auto-generated method stub
		return salesTransactionRepository.findBySaler(saler);
	}
	
	

}
