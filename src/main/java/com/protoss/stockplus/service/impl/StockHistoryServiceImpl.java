package com.protoss.stockplus.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.stockplus.entity.StockHistory;
import com.protoss.stockplus.repository.StockHistoryRepository;
import com.protoss.stockplus.service.StockHistoryService;

@Service
public class StockHistoryServiceImpl implements StockHistoryService{


	@Autowired
	StockHistoryRepository stockHistoryRepository;

	@Override
	public void save(StockHistory tockHistory) {
		// TODO Auto-generated method stub
		stockHistoryRepository.save(tockHistory);
		
	}

	
	

}
