﻿SELECT a.so_saler as saler,
       date_format(a.`so_sale_time`,'%Y%m%d') as saledate,
       SUM(a.so_subtotal_amt) as subtotalamount,
       SUM(a.so_rebate_amt) as rebateamount,
       SUM(a.so_vat_amt) as vatamount,
       SUM(a.so_total_amt) as totalamount,
       SUM(a.so_subtotal_amt_after_rebate) as subtotalafterrebate,
			 SUM(a.so_item_cost_amt) as itemcost,
       SUM(a.so_cont_amt) as contamount,
			 SUM(a.so_margin_amt) as marginamount
FROM tns_sale_order a
GROUP BY a.so_saler,date_format(a.`so_sale_time`,'%Y%m%d')
